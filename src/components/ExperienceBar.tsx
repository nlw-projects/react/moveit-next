import { useContext } from 'react'
import { ChallengeContext } from '../contexts/ChallengeContexts'
import styles from '../styles/components/ExperienceBar.module.css'

export function ExperienceBar() {
  const { currentExperience, experienceToNextLevel } = useContext(ChallengeContext);

  const percentoToNetLevel = Math.round(currentExperience * 100) / experienceToNextLevel;

  return (
    <div>
      <header className={styles.experienceBar}>
        <span>0 xp</span>
        <div>
          <div style={{width: `${percentoToNetLevel}%`}} />
          <span className={styles.currentExperience} style={{ left: `${percentoToNetLevel}%` }}>
            { currentExperience } xp
            </span>
        </div>
        <span>{ experienceToNextLevel } xp</span>
      </header>
    </div>
  )
}