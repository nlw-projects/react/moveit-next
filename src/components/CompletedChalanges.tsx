import { useContext } from 'react'
import { ChallengeContext } from '../contexts/ChallengeContexts'
import styles from '../styles/components/CompletedChalanges.module.css'

export function CompletedChalanges(){
  const {challengesCompleted} = useContext(ChallengeContext);
  return (
    <div className={styles.container}>
      <span>Desafios completos</span>
      <span>{ challengesCompleted }</span>
    </div>
  )
}